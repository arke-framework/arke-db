//
// Created by Dami on 01/07/2018.
//
#include <catch.hpp>

#include <arke/db/document/Document.hxx>
#include <arke/db/store/Store.hxx>
#include <arke/db/record/RecordCursor.hxx>
#include <arke/db/document/Collection.hxx>

using namespace arke::db;

TEST_CASE("Serialize document", "[document,record,serialize]") {

    Document subDoument1{};
    subDoument1.append("name", "doc1");

    Document subDoument2{};
    subDoument2.append("name", "doc2");

    Document document{};
    document.append("name", "name1");
    document.append("sub1", subDoument1);
    document.append("sub2", subDoument2);

    std::string id = document.oid().str();

    InMemoryStore store{};
    DocumentSerializer{store}.serialize(document);

    auto records = store.select();
    REQUIRE(records.count() == 6);
    auto cursor = records.cursor();

    auto recordOid = cursor.get();
    REQUIRE(recordOid->key() == id);

    cursor.next();
    auto recordName = cursor.get();
    REQUIRE(recordName->key() == id + ".name");
    REQUIRE(recordName->value()->str() == "name1");

    cursor.next();
    auto recordDoc1 = cursor.get();
    REQUIRE(recordDoc1->key() == id + ".sub1");
    REQUIRE(recordDoc1->value()->str() == subDoument1.oid().str());

    cursor.next();
    auto recordDoc1Name = cursor.get();
    REQUIRE(recordDoc1Name->key() == id + ".sub1.name");
    REQUIRE(recordDoc1Name->value()->str() == "doc1");

    cursor.next();
    auto recordDoc2 = cursor.get();
    REQUIRE(recordDoc2->key() == id + ".sub2");
    REQUIRE(recordDoc2->value()->str() == subDoument2.oid().str());

    cursor.next();
    auto recordDoc2Name = cursor.get();
    REQUIRE(recordDoc2Name->key() == id + ".sub2.name");
    REQUIRE(recordDoc2Name->value()->str() == "doc2");
}
