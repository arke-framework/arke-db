//
// Created by Dami on 02/07/2018.
//
#include <catch.hpp>

#include <arke/db/store/Store.hxx>
#include <arke/db/record/RecordCursor.hxx>
#include <arke/db/document/DocumentDeserializer.hxx>
#include <arke/db/document/OID.hxx>
#include <arke/db/value/OIDValue.hxx>

using namespace arke::db;

TEST_CASE("Deserialize document", "[document,record,deserialize]") {

    OID documentOidA{};
    OID documentOidB{};

    bool inverse = documentOidB.str() < documentOidA.str();

    OID& documentOid1 = inverse ? documentOidB : documentOidA;
    OID& documentOid2 = inverse ? documentOidA : documentOidB;

    OID sub1DocumentOid{};
    OID sub2DocumentOid{};

    auto oidStr1 = documentOid1.str();
    auto oidStr2 = documentOid2.str();
    auto sub1OidStr = sub1DocumentOid.str();

    InMemoryStore store{};

    store.add(Record{oidStr1, std::make_shared<OIDValue>(documentOid1, ValueType::OBJECT)});
    store.add(Record{oidStr1 + ".name", std::make_shared<StringValue>("Object name 1")});
    store.add(Record{oidStr1 + ".sub1", std::make_shared<OIDValue>(sub1DocumentOid, ValueType::OBJECT)});
    store.add(Record{oidStr1 + ".sub1.name", std::make_shared<StringValue>("sub1 name")});
    store.add(Record{oidStr1 + ".sub2", std::make_shared<OIDValue>(sub2DocumentOid, ValueType::OBJECT)});
    store.add(Record{oidStr1 + ".sub2.name", std::make_shared<StringValue>("sub2 name")});
    store.add(Record{oidStr2, std::make_shared<OIDValue>(documentOid2, ValueType::OBJECT)});

    auto query = store.select();
    REQUIRE(query.count() == 7);

    auto cursor = query.cursor();
    auto document1 = DocumentDeserializer{}.extractFromCursor(cursor);

    REQUIRE(document1->oid().str() == oidStr1);
    REQUIRE(document1->oid() == documentOid1);
    REQUIRE(document1->fields().size() == 3);
    REQUIRE(document1->at("name")->str() == "Object name 1");

    auto subObject1 = document1->at("sub1")->document();
    REQUIRE(subObject1.fields().size() == 1);
    REQUIRE(subObject1.at("name")->str() == "sub1 name");
    auto subObject2 = document1->at("sub2")->document();
    REQUIRE(subObject2.fields().size() == 1);
    REQUIRE(subObject2.at("name")->str() == "sub2 name");

    auto document2 = DocumentDeserializer{}.extractFromCursor(cursor);
    REQUIRE(document2->oid().str() == oidStr2);

    REQUIRE_FALSE(DocumentDeserializer{}.extractFromCursor(cursor));
}
