//
// Created by dami on 13/06/2018.
//

#include <catch.hpp>

#include <arke/db/record/Record.hxx>
#include <arke/db/value/StringValue.hxx>

using namespace arke::db;

TEST_CASE("Create empty record", "[record]") {
    auto key = "key1";
    auto record = Record{key};
    REQUIRE(key == record.key());
    REQUIRE(record.value()->empty());
    REQUIRE(record.value()->type() == ValueType::EMPTY);
}

TEST_CASE("Create string record", "[record]") {
    auto key = "key1";
    auto value = "value1";
    auto record = Record{key, value};
    REQUIRE(key == record.key());
    REQUIRE(record.value()->type() == ValueType::STRING);
    REQUIRE(value == record.value()->str());
}

TEST_CASE("Create record with existing value", "[record]") {
    auto key = "key1";
    auto str = "MyValue";
    auto recordValue = std::make_unique<StringValue>(str);
    auto record = Record{key, std::move(recordValue)};
    REQUIRE(key == record.key());
    REQUIRE(str == record.value()->str());
}
