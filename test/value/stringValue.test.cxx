//
// Created by dami on 13/06/2018.
//

#include <catch.hpp>
#include <arke/db/value/StringValue.hxx>

using namespace arke::db;

TEST_CASE("String value creation", "[value]") {
    auto str = "MyValue";
    StringValue value{str};
    REQUIRE(value.type() == ValueType::STRING);
    REQUIRE(value.str() == str);
    REQUIRE_FALSE(value.empty());
}

TEST_CASE("Update string creation", "[value]") {
    auto str = "MyValue";
    StringValue value{""};
    REQUIRE(value.str() == "");
    REQUIRE(value.str().empty());
    value.str(str);
    REQUIRE(value.str() == str);
    REQUIRE_FALSE(value.empty());
}
