//
// Created by Dami on 03/07/2018.
//
#include <catch.hpp>
#include <arke/db/value/DocumentValue.hxx>

using namespace arke::db;

TEST_CASE("Create document value", "[value, document]") {
    auto document = std::make_shared<Document>();
    DocumentValue value{document};

    REQUIRE_FALSE(value.empty());
}
