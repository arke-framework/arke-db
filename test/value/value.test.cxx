//
// Created by Dami on 23/06/2018.
//
#include <catch.hpp>
#include <arke/db/document/OID.hxx>
#include <arke/db/value/EmptyValue.hxx>
#include <arke/db/value/StringValue.hxx>

using namespace arke::db;

TEST_CASE("Clone empty value", "[value]") {
    EmptyValue value1{};
    auto value2 = clone(value1);
    REQUIRE(value2->type() == ValueType::EMPTY);
    REQUIRE(value2->empty());

    REQUIRE_THROWS_AS(value2->str(), std::runtime_error);
    REQUIRE_THROWS_AS(value2->oid(), std::runtime_error);
    REQUIRE_THROWS_AS(value2->document(), std::runtime_error);

    REQUIRE_THROWS_AS(value2->str("str"), std::runtime_error);
    REQUIRE_THROWS_AS(value2->oid(OID{}), std::runtime_error);

    Value& val = *value2;

    REQUIRE_THROWS_AS(val.str(), std::runtime_error);
    REQUIRE_THROWS_AS(val.oid(), std::runtime_error);
    REQUIRE_THROWS_AS(val.document(), std::runtime_error);

    REQUIRE_THROWS_AS(val.str("str"), std::runtime_error);
    REQUIRE_THROWS_AS(val.oid(OID{}), std::runtime_error);
}

TEST_CASE("Clone string value", "[value]") {
    auto str = "MyValue";
    StringValue value1{str};
    auto value2 = clone(value1);
    REQUIRE(value2->type() == ValueType::STRING);
    REQUIRE(value2->str() == str);
    REQUIRE_FALSE(value2->str().empty());
}
