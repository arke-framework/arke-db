//
// Created by Dami on 03/07/2018.
//

#include <catch.hpp>
#include <arke/db/value/OIDValue.hxx>

using namespace arke::db;

TEST_CASE("Create oid value", "[value, oid]") {
    OID oid{};
    OIDValue value1{oid};
    OIDValue value2{oid.str(), ValueType::OID};

    REQUIRE_FALSE(value1.empty());
    REQUIRE_FALSE(value2.empty());

    REQUIRE(oid.str() == value1.str());
    REQUIRE(oid.str() == value2.str());

    REQUIRE(oid == value1.oid());
    REQUIRE(oid == value2.oid());

    try {
        OIDValue(oid.str(), ValueType::STRING);
        FAIL();
    } catch (const std::runtime_error& exception) {
        SUCCEED();
    }
}

TEST_CASE("Update oid value", "[value, oid]") {
    OID oid1{};
    OID oid2{};
    OIDValue value1{oid1};
    REQUIRE(oid1.str() != oid2.str());
    REQUIRE(value1.str() == oid1.str());

    value1.oid(oid2);
    REQUIRE(value1.str() == oid2.str());

    value1.str(oid1.str());
    REQUIRE(value1.str() == oid1.str());
}
