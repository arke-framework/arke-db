//
// Created by Dami on 23/06/2018.
//
#include <catch.hpp>
#include <arke/db/value/EmptyValue.hxx>

using namespace arke::db;

TEST_CASE("Empty value creation", "[value]") {
    EmptyValue value{};
    REQUIRE(value.type() == ValueType::EMPTY);
    REQUIRE(value.empty());
    REQUIRE_THROWS(value.str());
}
TEST_CASE("Other empty value creation", "[value]") {
    EmptyValue value{ValueType::OID};
    REQUIRE(value.type() == ValueType::OID);
    REQUIRE(value.empty());
    REQUIRE_THROWS(value.str());
}
