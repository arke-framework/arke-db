//
// Created by Dami on 17/06/2018.
//
#include <catch.hpp>
#include <sstream>

#include <arke/db/document/OID.hxx>

using namespace arke::db;

TEST_CASE("Create OID", "[oid,document]") {
    auto oid = OID{};
    REQUIRE_FALSE(oid.str().empty());
}

TEST_CASE("Create OID from string", "[oid,document]") {
    auto oid1 = OID{};
    auto oid2 = OID{oid1.str()};
    REQUIRE(oid1.str() == oid2.str());
}

TEST_CASE("Write OID to stream", "[oid,document]") {
    std::stringstream ss;
    auto oid1 = OID{};
    ss << oid1.str();
    REQUIRE(oid1.str() == ss.str());
}
