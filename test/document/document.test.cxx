//
// Created by Dami on 17/06/2018.
//
#include <catch.hpp>
#include <arke/db/document/Document.hxx>
#include <arke/db/document/Field.hxx>

using namespace arke::db;

TEST_CASE("Create empty document", "[document]") {
    Document document{};
    REQUIRE_FALSE(document.oid().str().empty());
    REQUIRE(document.fields().empty());
}

TEST_CASE("Create document from fields set", "[document]") {
    std::set<FieldPtr> fields{};
    fields.insert(std::make_shared<Field>("field1"));
    Document document{fields};
    REQUIRE_FALSE(document.oid().str().empty());
    REQUIRE(document.fields().size() == 1);
}

TEST_CASE("Add field to document", "[document]") {
    Document document{};
    document.append("field1", "value1");
    document.append("field2", "value2");
    REQUIRE(document.fields().size() == 2);
}

TEST_CASE("Add existing field to document", "[document]") {
    Document document{};
    document.append("field1", "value1");
    REQUIRE_THROWS(document.append("field1", "value2"));
}

TEST_CASE("Get an existing field from document", "[document]") {
    Document document{};
    document.append("field1", "value1");
    auto field = document.at("field1");
    REQUIRE(field->name() == "field1");
}

TEST_CASE("Get an invalid field from document", "[document]") {
    Document document{};
    REQUIRE_THROWS(document.at("field1"));
}

TEST_CASE("Has on existing field", "[document]") {
    Document document{};
    document.append("field1", "value1");
    REQUIRE(document.has("field1"));
}

TEST_CASE("Has on invalid field", "[document]") {
    Document document{};
    REQUIRE_FALSE(document.has("field1"));
}

TEST_CASE("Remove an invalid field from document", "[document]") {
    Document document{};
    document.append("field1", "value1");
    document.remove("field1");
    REQUIRE_FALSE(document.has("field1"));
}
