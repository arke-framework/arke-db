//
// Created by Dami on 17/06/2018.
//
#include <catch.hpp>
#include <arke/db/document/Field.hxx>

using namespace arke::db;

TEST_CASE("Create empty Field", "[field,document]") {
    auto field = Field{"field1"};
    REQUIRE(field.name() == "field1");
    REQUIRE(field.empty());
}

TEST_CASE("Create string Field", "[field,document]") {
    auto field = Field{"field1", "value"};
    REQUIRE(field.name() == "field1");
    REQUIRE(field.str() == "value");
}

TEST_CASE("Field comparison", "[field,document]") {
    auto field1 = Field{"field1"};
    auto field2 = Field{"field2"};
    auto field3 = Field{"field3"};
    auto field4 = Field{"field4"};
    auto field5 = Field{"field5"};

    REQUIRE(field1 < field2);
    REQUIRE(field2 < field3);
    REQUIRE(field3 < field4);
    REQUIRE_FALSE(field5 < field4);
}
