//
// Created by Dami on 18/06/2018.
//
#include <catch.hpp>
#include <arke/db/document/Collection.hxx>

using namespace arke::db;

TEST_CASE("Create collection", "[document]") {
    Collection{std::make_unique<InMemoryStore>()};
}

TEST_CASE("Add simple document to collection", "[document]") {
    Document document{};
    auto store = std::make_unique<InMemoryStore>();
    auto storePtr = store.get();
    Collection collection{std::move(store)};
    collection.add(document);
    REQUIRE(storePtr->select().count() == 1);
}

TEST_CASE("Add document to collection", "[document]") {
    Document document{};
    document.append("name", "document1");
    document.append("description", "Document description");

    auto store = std::make_unique<InMemoryStore>();
    auto storePtr = store.get();
    Collection collection{std::move(store)};
    collection.add(document);
    REQUIRE(storePtr->select().count() == 3);
}

TEST_CASE("Get document from collection", "[document]") {
    Document document{};
    document.append("name", "document1");
    document.append("description", "Document description");

    Collection collection{std::make_unique<InMemoryStore>()};
    collection.add(Document{});
    collection.add(document);
    collection.add(Document{});

    auto retrieved = collection.get(document.oid());
    REQUIRE(document.oid() == retrieved->oid());
    REQUIRE(retrieved->fields().size() == 2);
    REQUIRE(retrieved->has("name"));
    REQUIRE(retrieved->at("name")->str() == "document1");
    REQUIRE(retrieved->has("description"));
    REQUIRE(retrieved->at("description")->str() == "Document description");
}

TEST_CASE("Get invalid document from collection", "[document]") {
    Collection collection{std::make_unique<InMemoryStore>()};
    REQUIRE_THROWS(collection.get(OID{}));
}

TEST_CASE("Get complex document from collection", "[document]") {
    Document document{};
    document.append("name", "sub1");
    Document subDoument1{};
    subDoument1.append("name", "doc1");
    document.append("sub1", subDoument1);

    Collection collection{std::make_unique<InMemoryStore>()};
    collection.add(document);

    auto retrieved = collection.get(document.oid());
    REQUIRE(document.oid() == retrieved->oid());
}

TEST_CASE("Remove document from collection", "[document]") {
    Document document1{};
    Document document2{};
    document2.append("name", "document1");
    document2.append("description", "Document description");
    Document subDocument21{};
    document2.append("sub1",subDocument21);
    Document subDocument211{};
    subDocument21.append("subsub1",subDocument211);
    Document subDocument22{};
    document2.append("sub2",subDocument21);
    Document document3{};

    auto storePtr = std::make_unique<InMemoryStore>();
    auto store = storePtr.get();
    Collection collection{std::move(storePtr)};
    collection.add(document1);
    collection.add(document2);
    collection.add(document3);

    REQUIRE(store->select().count() == 8);
    collection.remove(document2.oid());
    REQUIRE(store->select().count() == 2);
}

TEST_CASE("Update document from collection", "[document]") {

    Document document1{};
    document1.append("name", "document1");
    document1.append("description", "Document description");
    Document subDocument21{};
    document1.append("sub1",subDocument21);
    Document subDocument211{};
    subDocument21.append("subsub1",subDocument211);
    subDocument21.append("attr1", "value 1");
    subDocument21.append("attr2", "value 2");
    Document subDocument22{};
    document1.append("sub2",subDocument21);

    auto storePtr = std::make_unique<InMemoryStore>();
    auto store = storePtr.get();
    Collection collection{std::move(storePtr)};
    collection.add(document1);

    REQUIRE(document1.fields().size() == 4);

    auto subDocument1 = document1.at("sub2");
    REQUIRE(subDocument1);
    REQUIRE(subDocument1->document().fields().size() == 3);

    REQUIRE(store->select().count() == 8);
    document1.append("attr1", "value 1");
    document1.append("attr2", "value 2");
    auto& sub1 = document1.at("sub2")->document();
    sub1.remove("subsub1");
    sub1.append("attr3", "value 3");
    sub1.append("attr4", "value 4");
    sub1.append("attr5", "value 5");

    collection.update(document1);
    REQUIRE(store->select().count() == 12);
}

TEST_CASE("Foreach mutable collection", "[document]") {
    Document doc1{};
    doc1.append("name", "doc1");
    Document doc2{};
    doc2.append("name", "doc2");
    doc2.append("desc", "desc doc2");
    Document doc3{};
    doc3.append("name", "doc3");
    doc3.append("desc", "desc doc3");
    doc3.append("other", "other doc3");

    Collection collection{std::make_unique<InMemoryStore>()};
    collection.add(doc1);
    collection.add(doc2);
    collection.add(doc3);

    int result = 0;
    collection.foreach([&](DocumentPtr doc) {
        result += doc->fields().size() + 1;
        return true;
    });

    REQUIRE(result == 9);
}

TEST_CASE("Foreach constant collection", "[document]") {
    Document doc1{};
    doc1.append("name", "doc1");
    Document doc2{};
    doc2.append("name", "doc2");
    doc2.append("desc", "desc doc2");
    Document doc3{};
    doc3.append("name", "doc3");
    doc3.append("desc", "desc doc3");
    doc3.append("other", "other doc3");

    Collection collection{std::make_unique<InMemoryStore>()};
    collection.add(doc1);
    collection.add(doc2);
    collection.add(doc3);

    const Collection& constCollection = collection;

    int result = 0;
    constCollection.foreach([&](const Document& doc) {
        result += doc.fields().size() + 1;
    });

    REQUIRE(result == 9);
}

TEST_CASE("Foreach collection with condition", "[document]") {
    Document doc1{};
    doc1.append("name", "doc1");
    Document doc2{};
    doc2.append("name", "doc2");
    doc2.append("desc", "desc doc2");
    Document doc3{};
    doc3.append("name", "doc3");
    doc3.append("desc", "desc doc3");
    doc3.append("other", "other doc3");

    Collection collection{std::make_unique<InMemoryStore>()};
    collection.add(doc1);
    collection.add(doc2);
    collection.add(doc3);

    const Collection& constCollection = collection;

    int current = 0;
    int result = 0;
    constCollection.foreach([&](const DocumentPtr doc) {
        result += doc->fields().size() + 1;
        ++current;
        return current < 2;
    });

    REQUIRE(result > 0);
    REQUIRE(result < 9);
}
