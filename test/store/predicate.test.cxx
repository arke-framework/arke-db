//
// Created by Dami on 03/07/2018.
//
#include <catch.hpp>
#include <arke/db/record/RecordPredicate.hxx>
#include <arke/db/record/Record.hxx>

using namespace arke::db;

TEST_CASE("RecordPredicate", "[store,predicate]") {
    Record record{"key", "value"};
    REQUIRE(Predicate<Record>{}.test(record));
}
