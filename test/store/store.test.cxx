//
// Created by dami on 14/06/2018.
//
#include <catch.hpp>

#include <set>
#include <string>

#include <arke/db/engine/memory/InMemoryStoreEngine.hxx>
#include <arke/db/record/Record.hxx>
#include <arke/db/record/RecordQuery.hxx>
#include <arke/db/record/RecordCursor.hxx>

#include <boost/algorithm/string.hpp>

using namespace arke::db;

TEST_CASE("Create LevelDB store", "[store,leveldb]") {
    auto store = InMemoryStoreEngine{};
}

// --- Add ---

TEST_CASE("Add record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    store.add(Record{"key1", "value1"});
}

// --- Get ---

TEST_CASE("Get record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    store.add(Record{"key1", "value1"});
    auto record = store.get("key1");
    REQUIRE(record->value()->str() == "value1");
}

TEST_CASE("Get empty record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    REQUIRE_FALSE(store.get("key"));
}

// --- Remove ---

TEST_CASE("Remove existing record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    store.add(Record{"key1", "value1"});
    auto record1 = store.get("key1");
    REQUIRE(record1->value()->str() == "value1");
    store.remove("key1");
    REQUIRE_FALSE(store.get("key1"));
}

TEST_CASE("Remove empty record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    store.remove("key");
}

// --- Update ---

TEST_CASE("Update existing record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    store.add(Record{"key1", "value1"});
    auto record1 = store.get("key1");
    REQUIRE(record1->value()->str() == "value1");
    store.update(Record{"key1", "value2"});
    auto record2 = store.get("key1");
    REQUIRE(record2->value()->str() == "value2");
}

TEST_CASE("Update empty record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    REQUIRE_FALSE(store.get("key1"));
    store.update(Record{"key1", "value1"});
    auto record2 = store.get("key1");
    REQUIRE(record2->value()->str() == "value1");
}

// Foreach

#include <iostream>

TEST_CASE("For each records", "[store,leveldb]") {
    std::set<std::string> values{
        "key1",
        "key2",
        "key3"
    };

    InMemoryStore store{};
    store.add(Record{"key1", "value1"});
    store.add(Record{"key2", "value2"});
    store.add(Record{"key3", "value3"});

    store.foreach([&](const RecordPtr record) {
        values.erase(record->key());
        return true;
    });

    REQUIRE(values.empty());
}

TEST_CASE("Partial for each records", "[store,leveldb]") {
    int count = 0;
    std::set<std::string> values{
        "key11",
        "key12",
        "key21",
        "key22"
    };

    InMemoryStore store{};
    store.add(Record{"key11", "value1"});
    store.add(Record{"key12", "value2"});
    store.add(Record{"key21", "value3"});
    store.add(Record{"key22", "value4"});

    const auto& constStore = store;

    constStore.foreach([&](const RecordPtr record) {
        ++count;
        auto key = record->key();
        auto flag = boost::algorithm::starts_with(key, "key1");
        if (flag) {
            values.erase(key);
        }
        return flag;
    });

    REQUIRE(values.size() == 2);
    REQUIRE(count == 4);
}
