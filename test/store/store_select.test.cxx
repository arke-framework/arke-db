//
// Created by dami on 14/06/2018.
//

#include <catch.hpp>

#include <arke/db/engine/memory/InMemoryStoreEngine.hxx>
#include <arke/db/record/Record.hxx>
#include <arke/db/record/RecordQuery.hxx>
#include <arke/db/record/RecordCursor.hxx>

using namespace arke::db;

// --- Select ---

TEST_CASE("Select count empty record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    auto query = store.select();
    REQUIRE(query.count() == 0);
}

TEST_CASE("Select count record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    store.update(Record{"key1", "value1"});
    store.update(Record{"key2", "value2"});
    auto query = store.select();
    REQUIRE(query.count() == 2);
}

TEST_CASE("Select cursor empty record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    auto query = store.select();
    auto cursor = query.cursor();
    REQUIRE_FALSE(cursor.isValid());
    REQUIRE_FALSE(cursor.next());
}

TEST_CASE("Select cursor record", "[store,leveldb]") {
    auto store = InMemoryStore{};
    store.update(Record{"key1", "value1"});
    store.update(Record{"key2", "value2"});
    auto query = store.select();
    auto cursor = query.cursor();
    REQUIRE(cursor.isValid());
    auto record1 = cursor.get();
    REQUIRE("key1" == record1->key());
    REQUIRE(cursor.next());
    REQUIRE(cursor.isValid());
    auto record2 = cursor.get();
    REQUIRE("key2" == record2->key());
    REQUIRE_FALSE(cursor.next());
    REQUIRE_FALSE(cursor.isValid());
}

TEST_CASE("Select cursor record with predicate", "[store,leveldb]") {
    auto store = InMemoryStore{};
    store.update(Record{"key1", "value1"});
    store.update(Record{"key2", "okvalue2"});
    store.update(Record{"key3", "okvalue3"});
    store.update(Record{"key4", "value4"});
    store.update(Record{"key5", "okvalue5"});
    auto query = store.select(predicate<Record>([](const Record& record) {
        auto str =  record.value().str();
        return str.size() >= 2 && str[0] == 'o' && str[1] == 'k';
    }));
    auto cursor = query.cursor();
    REQUIRE(cursor.isValid());
    auto record1 = cursor.get();
    REQUIRE("key2" == record1->key());
    REQUIRE(cursor.next());
    REQUIRE(cursor.isValid());
    auto record2 = cursor.get();
    REQUIRE("key3" == record2->key());
    REQUIRE(cursor.next());
    REQUIRE(cursor.isValid());
    auto record3 = cursor.get();
    REQUIRE("key5" == record3->key());
    REQUIRE_FALSE(cursor.next());
    REQUIRE_FALSE(cursor.isValid());
}
