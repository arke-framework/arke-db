file(GLOB_RECURSE ARKE_SOURCE_FILES src/*.[hct]xx)
file(GLOB_RECURSE ARKE_TEST_FILES test/*.[hct]xx)
set(ALL_SOURCE_FILES ${ARKE_SOURCE_FILES} ${ARKE_TEST_FILES})

message(${ALL_SOURCE_FILES})

add_custom_target(uncrustify-check
    COMMAND uncrustify
    -c ${PROJECT_SOURCE_DIR}/cmake/.uncrustify
    --check
    ${ALL_SOURCE_FILES}
)

add_custom_target(uncrustify-fix
    COMMAND uncrustify
    --replace
    --no-backup
    -c ${PROJECT_SOURCE_DIR}/cmake/.uncrustify
    ${ALL_SOURCE_FILES}
)
