# Arke-DB document database

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/bb97055e13ff46a0b93a79ccbeeb3170)](https://www.codacy.com/app/damicreabox/arke-db?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=arke-framework/arke-db&amp;utm_campaign=Badge_Grade) [![codecov](https://codecov.io/gl/arke-framework/arke-db/branch/master/graph/badge.svg)](https://codecov.io/gl/arke-framework/arke-db)



Arke-DB is an embedded document store database engine

## Compilation

Init conan

```
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
```

Build with CMake

```
mkdir build && cd build
# Install dependencies
conan install .. --build=missing
# Init cmake
cmake ..
# build
cmake --build .
```
