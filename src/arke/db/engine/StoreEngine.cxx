//
// Created by Dami on 23/06/2018.
//
#include "StoreEngine.hxx"

#include "../record/Record.hxx"

namespace arke::db {

    void StoreEngine::add(const Record& record) {
        add(std::make_shared<Record>(record));
    }
    void StoreEngine::update(const Record& record) {
        update(std::make_shared<Record>(record));
    }
}
