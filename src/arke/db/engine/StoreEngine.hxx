//
// Created by dami on 16/06/2018.
//

#ifndef ARKE_DB_STOREENGINE_HXX
#define ARKE_DB_STOREENGINE_HXX

#include "../record/Record.hxx"
#include "../record/RecordQuery.hxx"
#include "../record/RecordPredicate.hxx"

namespace arke::db {
    struct StoreEngine {
        virtual ~StoreEngine() {}
        virtual RecordPtr get(const std::string& key) const = 0;
        virtual void add(const Record& record);
        virtual void add(RecordPtr record) = 0;
        virtual void update(const Record& record);
        virtual void update(RecordPtr record) = 0;
        virtual void remove(const std::string& key) = 0;
        virtual void foreach(std::function<bool(RecordPtr)>) = 0;
        virtual void foreach(std::function<bool(const RecordPtr)>) const = 0;

        virtual std::unique_ptr<QueryEngine> getQuery(std::unique_ptr<const Predicate<Record> > predicate) const = 0;
    };
}

#endif //ARKE_DB_STOREENGINE_HXX
