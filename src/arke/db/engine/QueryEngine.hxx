//
// Created by dami on 16/06/2018.
//

#ifndef ARKE_DB_QUERYENGINE_HXX
#define ARKE_DB_QUERYENGINE_HXX

#include <cstddef>
#include "CursorEngine.hxx"

namespace arke::db {
    struct QueryEngine {
        virtual ~QueryEngine() {}
        virtual size_t count() const = 0;
        virtual std::unique_ptr<CursorEngine> getCursor() const = 0;
    };
}

#endif //ARKE_DB_QUERYENGINE_HXX
