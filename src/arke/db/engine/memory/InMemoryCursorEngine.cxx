//
// Created by Dami on 16/06/2018.
//

#include "InMemoryCursorEngine.hxx"

namespace arke::db {
    InMemoryCursorEngine::InMemoryCursorEngine(
        std::shared_ptr<const Predicate<Record> > predicate,
        InMemoryStoreEngine::CollectionType::const_iterator current,
        InMemoryStoreEngine::CollectionType::const_iterator end):
      predicate_(predicate), current_(current), end_(end) {
        if (!predicateValid()) {
            next();
        }
    }

    bool InMemoryCursorEngine::predicateValid() const {
        if (isValid()) {
            return !predicate_ || predicate_->test(
                Record{current_->first, current_->second});
        }
        return false;
    }

    bool InMemoryCursorEngine::isValid() const {
        return current_ != end_;
    }

    bool InMemoryCursorEngine::next() {
        if (isValid()) {
            ++current_;
            bool valid = isValid();
            if (valid && !predicateValid()) {
                return next();
            }
            return valid;
        }
        return false;
    }

    RecordPtr InMemoryCursorEngine::get() const {
        if (!isValid()) {
            throw std::out_of_range{"Invalid cursor position, out of result range"};
        }

        return std::make_shared<Record>(current_->first, current_->second);
    }
}
