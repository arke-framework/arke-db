//
// Created by dami on 16/06/2018.
//

#include "InMemoryStoreEngine.hxx"

#include "../../record/Record.hxx"
#include "InMemoryQueryEngine.hxx"

namespace arke::db {
    RecordPtr InMemoryStoreEngine::get(const std::string& key) const {
        try {
            return std::make_shared<Record>(key, values_.at(key));
        } catch(...) {
            return std::shared_ptr<Record>{};
        }
    }

    std::unique_ptr<QueryEngine> InMemoryStoreEngine::getQuery(std::unique_ptr<const Predicate<Record> > predicate) const {
        return std::make_unique<InMemoryQueryEngine>(*this, std::move(predicate));
    }

    void InMemoryStoreEngine::add(RecordPtr record) {
        values_[record->key()] = record->value();
    }

    void InMemoryStoreEngine::update(RecordPtr record) {
        values_[record->key()] = record->value();
    }

    void InMemoryStoreEngine::remove(const std::string& key) {
        values_.erase(key);
    }

    void InMemoryStoreEngine::foreach(std::function<bool(RecordPtr)> process) {
        for (auto&& [first,second] : this->values_) {
            process(std::make_shared<Record>(first, second));
        }
    }

    void InMemoryStoreEngine::foreach(std::function<bool(const RecordPtr)> process) const {
        for (auto&& [first,second] : this->values_) {
            process(std::make_shared<Record>(first, second));
        }
    }
}
