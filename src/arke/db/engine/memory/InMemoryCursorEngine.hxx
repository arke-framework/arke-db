//
// Created by Dami on 16/06/2018.
//

#ifndef ARKE_DB_INMEMORYCURSORENGINE_HXX
#define ARKE_DB_INMEMORYCURSORENGINE_HXX

#include "../CursorEngine.hxx"
#include "InMemoryStoreEngine.hxx"

namespace arke::db {
    class InMemoryCursorEngine : public CursorEngine {
        private:
            std::shared_ptr<const Predicate<Record> > predicate_;
            InMemoryStoreEngine::CollectionType::const_iterator current_;
            InMemoryStoreEngine::CollectionType::const_iterator end_;

            bool predicateValid() const;

        public:
            InMemoryCursorEngine(
                std::shared_ptr<const Predicate<Record> > predicate,
                InMemoryStoreEngine::CollectionType::const_iterator current,
                InMemoryStoreEngine::CollectionType::const_iterator end);
            virtual bool isValid() const override;
            virtual bool next() override;
            virtual RecordPtr get() const override;
    };
}

#endif //ARKE_DB_INMEMORYCURSORENGINE_HXX
