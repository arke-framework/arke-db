//
// Created by dami on 16/06/2018.
//

#ifndef ARKE_DB_INMEMORYQUERYENGINE_HXX
#define ARKE_DB_INMEMORYQUERYENGINE_HXX

#include <memory>
#include "../QueryEngine.hxx"
#include "../../record/RecordPredicate.hxx"

namespace arke::db {
    class InMemoryStoreEngine;

    template <typename T>
    struct Predicate;

    class InMemoryQueryEngine : public QueryEngine {
        private:
            const InMemoryStoreEngine& storeEngine_;
            std::shared_ptr<const Predicate<Record> > predicate_;

        public:
            explicit InMemoryQueryEngine(const InMemoryStoreEngine& storeEngine, std::unique_ptr<const Predicate<Record> > predicate);

            virtual size_t count() const override;
            virtual std::unique_ptr<CursorEngine> getCursor() const override;
    };
}

#endif //ARKE_DB_INMEMORYQUERYENGINE_HXX
