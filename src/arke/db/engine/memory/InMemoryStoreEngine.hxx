//
// Created by dami on 16/06/2018.
//

#ifndef ARKE_DB_INMEMORYSTOREENGINE_HXX
#define ARKE_DB_INMEMORYSTOREENGINE_HXX

#include <map>

#include "../../store/Store.hxx"
#include "../StoreEngine.hxx"

namespace arke::db {

    class InMemoryStoreEngine : public StoreEngine {
        friend class InMemoryQueryEngine;

        public:
            using DataType = std::shared_ptr<Value>;
            using CollectionType = std::map<std::string, DataType>;

        private:
            CollectionType values_;

        public:
            RecordPtr get(const std::string& key) const override;
            std::unique_ptr<QueryEngine> getQuery(std::unique_ptr<const Predicate<Record> > predicate) const override;
            void add(RecordPtr record) override;
            void update(RecordPtr record) override;
            void remove(const std::string& key) override;
            void foreach(std::function<bool(RecordPtr)> process) override;
            void foreach(std::function<bool(const RecordPtr)> process) const override;
    };
}

#endif //ARKE_DB_INMEMORYSTOREENGINE_HXX
