//
// Created by dami on 16/06/2018.
//

#include "InMemoryQueryEngine.hxx"

#include "InMemoryStoreEngine.hxx"
#include "InMemoryCursorEngine.hxx"

namespace arke::db {
    InMemoryQueryEngine::InMemoryQueryEngine(
        const InMemoryStoreEngine& storeEngine,
        std::unique_ptr<const Predicate<Record> > predicate):
      storeEngine_(std::move(storeEngine)),
      predicate_(std::move(predicate)) {}

    size_t InMemoryQueryEngine::count() const {
        size_t count = 0;
        for (auto cursor = getCursor(); cursor->isValid(); cursor->next()) {
            ++count;
        }
        return count;
    }

    std::unique_ptr<CursorEngine> InMemoryQueryEngine::getCursor() const {
        return std::make_unique<InMemoryCursorEngine>(
            predicate_,
            this->storeEngine_.values_.begin(),
            this->storeEngine_.values_.end());
    }
}
