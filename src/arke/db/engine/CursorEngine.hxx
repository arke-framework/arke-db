//
// Created by Dami on 16/06/2018.
//

#ifndef ARKE_DB_CURSORENGINE_HXX
#define ARKE_DB_CURSORENGINE_HXX

#include "../record/Record.hxx"

namespace arke::db {
    struct CursorEngine {
        virtual ~CursorEngine() {}

        virtual bool isValid() const = 0;
        virtual bool next() = 0;
        virtual RecordPtr get() const = 0;
    };
}

#endif //ARKE_DB_CURSORENGINE_HXX
