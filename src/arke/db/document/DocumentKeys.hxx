//
// Created by Dami on 12/07/2018.
//

#ifndef ARKE_DB_DOCUMENTKEYS_HXX
#define ARKE_DB_DOCUMENTKEYS_HXX

#include <string>
#include <set>

namespace arke::db {
    class Document;
    std::set<std::string> getFullKeys(const Document &document);
}

#endif //ARKE_DB_DOCUMENTKEYS_HXX
