//
// Created by Dami on 17/06/2018.
//

#include "Field.hxx"
#include "../value/StringValue.hxx"
#include "../value/EmptyValue.hxx"
#include "../value/DocumentValue.hxx"

namespace arke::db {

    Field::Field(const std::string &name): Field(name, std::make_unique<EmptyValue>()) {}

    Field::Field(const std::string &name, const std::string &value):
      Field(name, std::make_unique<StringValue>(value)) {}

    Field::Field(const std::string &name, DocumentPtr document):
      Field(name, std::make_unique<DocumentValue>(document)) {}

    Field::Field(const std::string &name, std::shared_ptr<Value> value):
      name_(name), value_(std::move(value)) {}

    Field::Field(Field&& field): name_(std::move(field.name_)), value_(std::move(field.value_)) {}

    const std::string &Field::name() const {
        return name_;
    }

    ValueType Field::type() const {
        return value_->type();
    }

    bool operator<(const Field& first, const Field& second) noexcept {
        return first.name_ < second.name_;
    }

    bool Field::empty() const {
        return value_->empty();
    }

    std::string Field::str() const {
        return value_->str();
    }

    void Field::str(const std::string &value) {
        value_->str(value);
    }

    const Document& Field::document() const {
        return value_->document();
    }

    Document& Field::document() {
        return value_->document();
    }
}
