//
// Created by Dami on 17/06/2018.
//

#ifndef ARKE_DB_FIELD_HXX
#define ARKE_DB_FIELD_HXX

#include <string>
#include "../value/ValueType.hxx"
#include "../value/Value.hxx"

namespace arke::db {

    class Field;
    using FieldPtr = std::shared_ptr<Field>;

    class Document;
    using DocumentPtr = std::shared_ptr<Document>;

    struct RecordValue;
    class Field {
        friend bool operator<(const Field&,const Field&) noexcept;

        private:
            std::string name_;
            std::shared_ptr<Value> value_;

        public:
            explicit Field(const std::string& name);
            explicit Field(const std::string& name, const std::string& value);
            explicit Field(const std::string& name, DocumentPtr document);
            explicit Field(const std::string& name, std::shared_ptr<Value> value);
            Field(Field&& field);

            const std::string& name() const;
            ValueType type() const;

            bool empty() const;

            std::string str() const;
            void str(const std::string& value);

            const Document& document() const;
            Document& document();
    };

    bool operator<(const Field& first,const Field& second) noexcept;
}

#endif //ARKE_DB_FIELD_HXX
