//
// Created by Dami on 18/06/2018.
//

#include "Collection.hxx"

#include "../record/RecordCursor.hxx"
#include <boost/algorithm/string.hpp>

namespace arke::db {

    Collection::Collection(std::unique_ptr<Store> store): store_(std::move(store)), serializer_{*store_} {}

    DocumentPtr Collection::get(const OID &oid) const {
        auto oidStr = oid.str();
        auto cursor = store_->select(predicate([&](const Record& record){
            return boost::algorithm::starts_with(record.key(), oidStr);
        })).cursor();
        if (!cursor.isValid()) {
            throw std::runtime_error{"Unable to find object " + oid.str()};
        }
        return deserializer_.extractFromCursor(cursor);
    }

    void Collection::add(const Document &document) {
        serializer_.serialize(document);
    }

    void Collection::update(const Document &document) {

        auto current = get(document.oid());
        if (!current) {
            throw std::runtime_error{"Object not exists " + document.oid().str()};
        }
        auto currentKeys = getFullKeys(*current);
        auto newKeys = getFullKeys(document);

        for (auto key: newKeys) {
            currentKeys.erase(key);
        }
        for (auto& key : currentKeys) {
            store_->remove(key);
        }
        serializer_.serialize(document, currentKeys);
    }

    void Collection::remove(const OID &oid) {
        auto object = get(oid);
        if(!object) {
            return;
        }

        for (const auto& key : getFullKeys(*object)) {
            store_->remove(key);
        }
    }

    void Collection::foreach(std::function<bool(DocumentPtr)> process) {
        auto cursor = this->store_->select().cursor();
        while (auto document = DocumentDeserializer{}.extractFromCursor(cursor)) {
            if(!process(document)) {
                break;
            }
        }
    }

    void Collection::foreach(std::function<bool(const DocumentPtr)> process) const {
        auto cursor = this->store_->select().cursor();
        while (auto document = DocumentDeserializer{}.extractFromCursor(cursor)) {
            if(!process(document)) {
                break;
            }
        }
    }

    void Collection::foreach(std::function<void(const Document&)> process) const {
        auto cursor = this->store_->select().cursor();
        while (auto document = DocumentDeserializer{}.extractFromCursor(cursor)) {
            process(*document);
        }
    }

    Query<Document> Collection::select(std::unique_ptr<const Predicate<Document> > predicate) const {
        throw std::runtime_error{"Not implemented"};
    }
}
