//
// Created by Dami on 17/06/2018.
//

#include "Document.hxx"

namespace arke::db {

    Document::Document(const OID &oid, const std::set<FieldPtr>& fields): oid_(oid) {
        for (auto& field : fields) {
            fields_.insert(field);
            fieldsMap_[field->name()] = field;
        }
    }

    Document::Document(const std::set<FieldPtr>& fields): Document( OID {}, fields) {}

    const OID &Document::oid() const {
        return oid_;
    }

    const std::set<Document::FieldPtr>& Document::fields() const {
        return fields_;
    }

    const Document::FieldPtr Document::at(const std::string& key) const {
        return fieldsMap_.at(key);
    }

    bool Document::has(const std::string &key) const {
        return fieldsMap_.find(key) != fieldsMap_.end();
    }

    void Document::append(const std::string& name, const std::string& value) {
        append(std::make_shared<Field>(name, value));
    }

    void Document::append(const std::string &name, const Document &value) {
        append(name, std::make_shared<Document>(value));
    }

    void Document::append(const std::string &name, DocumentPtr value) {
        append(std::make_shared<Field>(name, value));
    }

    void Document::append(FieldPtr field) {
        if (has(field->name())) {
            throw std::runtime_error{"Element already exist"};
        }
        fields_.insert(field);
        fieldsMap_[field->name()] = field;
    }

    void Document::remove(const std::string &key) {
        auto field = fieldsMap_.at(key);
        fields_.erase(field);
        fieldsMap_.erase(key);
    }
}
