//
// Created by Dami on 17/06/2018.
//

#ifndef ARKE_DB_OID_HXX
#define ARKE_DB_OID_HXX

#include <string>
#include <boost/uuid/uuid.hpp>

namespace arke::db {
    class OID {
        friend bool operator==(const OID&,const OID&) noexcept;

        private:
            boost::uuids::uuid uuid_;

        public:
            explicit OID();
            explicit OID(const std::string& uuid);
            std::string str() const;
    };

    bool operator==(const OID& first,const OID& second) noexcept;
    std::ostream& operator<<(std::ostream& stream, const OID& oid) noexcept;
}

#endif //ARKE_DB_OID_HXX
