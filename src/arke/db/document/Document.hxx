//
// Created by Dami on 17/06/2018.
//

#ifndef ARKE_DB_DOCUMENT_HXX
#define ARKE_DB_DOCUMENT_HXX

#include <memory>
#include <map>
#include <set>
#include "OID.hxx"
#include "Field.hxx"
#include "DocumentKeys.hxx"

namespace arke::db {
    class Document;
    using DocumentPtr = std::shared_ptr<Document>;

    class Document {
        public:
            using FieldPtr = std::shared_ptr<Field>;

        private:
            OID oid_;
            std::set<FieldPtr> fields_;
            std::map<std::string, FieldPtr> fieldsMap_;

        public:
            explicit Document(const std::set<FieldPtr>& fields = {});
            explicit Document(const OID& oid, const std::set<FieldPtr>& fields = {});

            const OID& oid() const;

            const std::set<FieldPtr>& fields() const;

            const FieldPtr at(const std::string& key) const;
            bool has(const std::string& key) const;
            void append(FieldPtr field);
            void append(const std::string& name, const std::string& value);
            void append(const std::string& name, DocumentPtr value);
            void append(const std::string& name, const Document& value);
            void remove(const std::string& key);
    };
}

#endif //ARKE_DB_DOCUMENT_HXX
