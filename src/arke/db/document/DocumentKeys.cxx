//
// Created by Dami on 12/07/2018.
//

#include "DocumentKeys.hxx"

#include "Document.hxx"

namespace arke::db {

    inline void getFullKeys(std::set<std::string> &keys,
                            const std::string &parent,
                            const Document &document) {
        for (const auto& field : document.fields()) {
            auto key = parent + "." + field->name();
            keys.insert(key);
            if (field->type() == ValueType::OBJECT) {
                getFullKeys(keys, key, field->document());
            }
        }
    }

    std::set<std::string> getFullKeys(const Document &document) {
        std::set<std::string> keys{};
        auto key = document.oid().str();
        keys.insert(key);
        getFullKeys(keys, key, document);
        return keys;
    }
}
