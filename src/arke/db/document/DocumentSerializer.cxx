//
// Created by dami on 18/06/2018.
//

#include "DocumentSerializer.hxx"

#include "../store/Store.hxx"
#include "Document.hxx"

namespace arke::db {

    DocumentSerializer::DocumentSerializer(Store& store): store_(store) { }

    void DocumentSerializer::serialize(
        const Document& document,
        std::set<std::string> keys,
        const std::string& path) {

        auto oid = document.oid();
        auto objectUid = oid.str();

        std::string fullPath;
        if (path.empty()) {
            fullPath = objectUid;
        } else {
            fullPath = path;
        }

        Record record{fullPath, oid, ValueType::OBJECT};
        if (keys.find(fullPath) == keys.end()) {
            store_.add(record);
        } else {
            store_.update(record);
        }

        for (auto& field : document.fields()) {
            insert(fullPath, keys, *field);
        }
    }

    void DocumentSerializer::insert(
        const std::string& parent,
        std::set<std::string> keys,
        const Field& field) {

        auto name = parent + "." + field.name();
        switch (field.type()) {
            case ValueType::STRING:
                store_.add(Record{name, field.str()});
                break;
            //case ValueType::OID:
            //    store_.add(Record{name, field.str(), field.type()});
            //    break;
            case ValueType::OBJECT:
                serialize(field.document(), keys, name);
                break;

            default:
                throw std::runtime_error{"Unknown document field type"};
        }
    }
}
