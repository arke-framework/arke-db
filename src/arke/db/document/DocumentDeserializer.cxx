//
// Created by dami on 18/06/2018.
//

#include "DocumentDeserializer.hxx"

#include "../record/RecordCursor.hxx"

namespace arke::db {

    DocumentPtr DocumentDeserializer::extractDocument(Cursor<Record> &cursor) const {

        if (!cursor.isValid()) {
            return DocumentPtr{};
        }

        auto record = cursor.get();
        auto value = record->value();
        if (value->type() != ValueType::OBJECT) {
            throw std::runtime_error{"Not an object"};
        }

        auto document = std::make_shared<Document>(value->oid());
        size_t keyLength = record->key().length();

        while(cursor.next()) {
            if (!extractField(cursor, keyLength, *document)) {
                break;
            }
        }

        return document;
    }

    bool DocumentDeserializer::extractField(Cursor<Record> &cursor, size_t keyLength, Document& document) const {
        if (!cursor.isValid()) {
            return false;
        }

        auto record = cursor.get();
        size_t length = record->key().length();
        if (length <= keyLength) {
            return false;
        }

        auto value = record->value();
        auto name = record->key().substr(keyLength + 1);

        if(value->type() == ValueType::OBJECT) {
            auto subDocument = extractDocument(cursor);
            if (subDocument) {
                document.append(std::make_shared<Field>(name, subDocument));
                return extractField(cursor, keyLength, document);
            }
            return false;
        } else {
            document.append(std::make_shared<Field>(name, value));
            return true;
        }
    }

    DocumentPtr DocumentDeserializer::extractFromCursor(Cursor<Record>& cursor) const {
        return extractDocument(cursor);
    }
}
