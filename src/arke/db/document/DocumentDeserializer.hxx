//
// Created by dami on 18/06/2018.
//

#ifndef ARKE_DB_DOCUMENTDESERIALIZER_HXX
#define ARKE_DB_DOCUMENTDESERIALIZER_HXX

#include "Document.hxx"

namespace arke::db {

    class Record;

    template <typename T>
    struct Cursor;

    class DocumentDeserializer {
        private:
            bool extractField(Cursor<Record>& cursor, size_t keyLength, Document& document) const;
            DocumentPtr extractDocument(Cursor<Record>& cursor) const;

        public:
            DocumentPtr extractFromCursor(Cursor<Record>& cursor) const;
    };
}

#endif //ARKE_DB_DOCUMENTDESERIALIZER_HXX
