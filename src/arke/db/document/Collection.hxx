//
// Created by Dami on 18/06/2018.
//

#ifndef ARKE_DB_COLLECTION_HXX
#define ARKE_DB_COLLECTION_HXX

#include <memory>

#include "../store/Store.hxx"
#include "Document.hxx"
#include "DocumentDeserializer.hxx"
#include "DocumentSerializer.hxx"

namespace arke::db {
    class Collection {
        private:
            std::unique_ptr<Store> store_;
            DocumentDeserializer deserializer_;
            DocumentSerializer serializer_;

        public:
            explicit Collection(std::unique_ptr<Store> store);

            void add(const Document &document);
            DocumentPtr get(const OID& oid) const;
            void update(const Document& document);
            void remove(const OID& oid);

            void foreach(std::function<bool(DocumentPtr)> process);
            void foreach(std::function<bool(const DocumentPtr)> process) const;
            void foreach(std::function<void(const Document&)> process) const;

            Query<Document> select(std::unique_ptr<const Predicate<Document> > predicate) const;
    };
}

#endif //ARKE_DB_COLLECTION_HXX
