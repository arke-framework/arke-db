//
// Created by Dami on 17/06/2018.
//

#include "OID.hxx"

#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace arke::db {

    OID::OID(): uuid_(boost::uuids::random_generator()()) {}

    OID::OID(const std::string &uuid): uuid_(boost::uuids::string_generator {}
                                             (uuid)) {}

    std::string OID::str() const {
        return boost::uuids::to_string(uuid_);
    }

    bool operator==(const OID& first, const OID& second) noexcept {
        return first.uuid_ == second.uuid_;
    }

    std::ostream& operator<<(std::ostream& stream, const OID& oid) noexcept {
        stream << "{" << oid.str() << "}";
        return stream;
    }
}
