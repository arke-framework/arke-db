//
// Created by dami on 18/06/2018.
//

#ifndef ARKE_DB_DOCUMENTSERIALIZER_HXX
#define ARKE_DB_DOCUMENTSERIALIZER_HXX

#include <string>
#include <set>

namespace arke::db {
    class Store;
    class Document;
    class Field;
    class DocumentSerializer {
        private:
            Store& store_;
            void insert(const std::string& parent,
                        std::set<std::string> keys,
                        const Field& field);

        public:
            // Store must be valid during entire serializer life cycle
            DocumentSerializer(Store& store);

            void serialize(
                const Document& document,
                std::set<std::string> keys = {},
                const std::string& path = {});
    };
}

#endif //ARKE_DB_DOCUMENTSERIALIZER_HXX
