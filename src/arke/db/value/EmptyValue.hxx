//
// Created by Dami on 22/06/2018.
//

#ifndef ARKE_DB_EMPTYVALUE_HXX
#define ARKE_DB_EMPTYVALUE_HXX

#include "Value.hxx"
#include "ValueType.hxx"

namespace arke::db {
    class EmptyValue : public Value {
        private:
            ValueType type_;

        public:
            explicit EmptyValue(ValueType type = ValueType::EMPTY);

            virtual ValueType type() const override;
            virtual bool empty() const override;
    };
}

#endif //ARKE_DB_EMPTYVALUE_HXX
