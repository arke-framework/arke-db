//
// Created by Dami on 22/06/2018.
//
#include "Value.hxx"
#include "ValueType.hxx"
#include "StringValue.hxx"
#include "EmptyValue.hxx"
#include "OIDValue.hxx"

namespace arke::db {

    ValuePtr clone(const Value& value) {
        switch (value.type()) {
            case ValueType::STRING:
                return std::make_shared<StringValue>(value.str());
            case ValueType::OID:
            case ValueType::OBJECT:
                return std::make_shared<OIDValue>(value.str(), value.type());
            case ValueType::EMPTY:
                return std::make_shared<EmptyValue>(value.type());
        }
        throw std::runtime_error{"Unknown type"};
    }

    const std::string &Value::str() const {
        throw std::runtime_error{"To str not implemented"};
    }

    void Value::str(const std::string &value) {
        throw std::runtime_error{"Set str not implemented"};
    }

    const OID &Value::oid() {
        throw std::runtime_error{"To OID not implemented"};
    }

    void Value::oid(const OID &oid) {
        throw std::runtime_error{"Set OID not implemented"};
    }

    Document &Value::document() {
        throw std::runtime_error{"To document not implemented"};
    }

    const Document &Value::document() const {
        throw std::runtime_error{"To document not implemented"};
    }
}
