//
// Created by Dami on 22/06/2018.
//

#include "EmptyValue.hxx"

namespace arke::db {
    EmptyValue::EmptyValue(ValueType type): type_(type) {}

    ValueType EmptyValue::type() const {
        return type_;
    }

    bool EmptyValue::empty() const {
        return true;
    }
}
