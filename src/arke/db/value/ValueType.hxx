//
// Created by Dami on 20/06/2018.
//

#ifndef ARKE_DB_VALUETYPE_HXX
#define ARKE_DB_VALUETYPE_HXX

enum class ValueType : short {
    EMPTY,
    STRING,
    OID,
    OBJECT
};

#endif //ARKE_DB_VALUETYPE_HXX
