//
// Created by Dami on 22/06/2018.
//

#include "StringValue.hxx"
#include "ValueType.hxx"

namespace arke::db {

    StringValue::StringValue(const std::string &value): value_(value) {}

    bool StringValue::empty() const {
        return value_.empty();
    }

    const std::string &StringValue::str() const {
        return value_;
    }

    void StringValue::str(const std::string &value) {
        this->value_ = value;
    }

    ValueType StringValue::type() const {
        return ValueType::STRING;
    }
}
