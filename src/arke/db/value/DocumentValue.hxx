//
// Created by Dami on 23/06/2018.
//

#ifndef ARKE_DB_DOCUMENTVALUE_HXX
#define ARKE_DB_DOCUMENTVALUE_HXX

#include "../document/Document.hxx"

namespace arke::db {
    class DocumentValue : public Value {
        private:
            DocumentPtr document_;

        public:
            explicit DocumentValue(DocumentPtr document);

            virtual bool empty() const override;
            virtual ValueType type() const override;

            virtual const Document& document() const override;
            virtual Document& document() override;
    };
}

#endif //ARKE_DB_DOCUMENTVALUE_HXX
