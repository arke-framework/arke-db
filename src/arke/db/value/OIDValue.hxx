//
// Created by Dami on 01/07/2018.
//

#ifndef ARKE_DB_OIDVALUE_HXX
#define ARKE_DB_OIDVALUE_HXX

#include <string>

#include "StringValue.hxx"
#include "../document/OID.hxx"

namespace arke::db {

    class OIDValue : public Value {
        private:
            OID oid_;
            mutable std::unique_ptr<std::string> str_;
            ValueType type_;

        public:
            bool empty() const override;

        public:
            OIDValue(const std::string& value, ValueType type_);
            OIDValue(const OID& value, ValueType type = ValueType::OID);

            virtual ValueType type() const override;

            virtual const OID& oid() override;
            virtual void oid(const OID& oid) override;

            virtual const std::string& str() const override;
            virtual void str(const std::string& value) override;
    };
}

#endif //ARKE_DB_OIDVALUE_HXX
