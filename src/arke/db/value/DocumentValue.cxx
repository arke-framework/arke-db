//
// Created by Dami on 23/06/2018.
//

#include "DocumentValue.hxx"

namespace arke::db {

    DocumentValue::DocumentValue(DocumentPtr document): document_(document) {}

    bool DocumentValue::empty() const {
        return !static_cast<bool>(document_);
    }

    ValueType DocumentValue::type() const {
        return ValueType::OBJECT;
    }

    const Document &DocumentValue::document() const {
        return *document_;
    }

    Document &DocumentValue::document() {
        return *document_;
    }
}
