//
// Created by Dami on 22/06/2018.
//

#ifndef ARKE_DB_VALUE_HXX
#define ARKE_DB_VALUE_HXX

#include <string>
#include <memory>
#include "ValueType.hxx"

namespace arke::db {
    class Document;
    class OID;
    struct Value;
    using ValuePtr = std::shared_ptr<Value>;
    using ValueConstPtr = std::shared_ptr<const Value>;
    struct Value {
        virtual ~Value() {}

        virtual bool empty() const = 0;
        virtual ValueType type() const = 0;

        virtual const std::string& str() const;
        virtual void str(const std::string& value);

        virtual const OID& oid();
        virtual void oid(const OID& oid);

        virtual Document& document();
        virtual const Document& document() const;
    };

    ValuePtr clone(const Value& value);
}

#endif //ARKE_DB_VALUE_HXX
