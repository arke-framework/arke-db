//
// Created by Dami on 22/06/2018.
//

#ifndef ARKE_DB_STRINGVALUE_HXX
#define ARKE_DB_STRINGVALUE_HXX

#include "Value.hxx"

namespace arke::db {
    class StringValue : public Value {
        private:
            std::string value_;

        public:
            explicit StringValue(const std::string& value);

            virtual ValueType type() const override;
            virtual bool empty() const override;

            virtual const std::string& str() const override;
            virtual void str(const std::string& value) override;
    };
}

#endif //ARKE_DB_STRINGVALUE_HXX
