//
// Created by Dami on 01/07/2018.
//

#include "OIDValue.hxx"

#include "../document/OID.hxx"

namespace arke::db {

    OIDValue::OIDValue(const OID& value, ValueType type):
      oid_(value), type_(type) {
        if (type != ValueType::OID && type != ValueType::OBJECT) {
            throw std::runtime_error{"Unknown type for OID"};
        }
    }

    OIDValue::OIDValue(const std::string &value, ValueType type):
      OIDValue(OID{value}, type) {}

    ValueType OIDValue::type() const {
        return type_;
    }

    const OID &OIDValue::oid() {
        return oid_;
    }

    void OIDValue::oid(const OID &oid) {
        oid_ = oid;
        str_.reset();
    }

    const std::string &OIDValue::str() const {
        if (!str_) {
            str_ = std::make_unique<std::string>(oid_.str());
        }
        return *str_;
    }

    void OIDValue::str(const std::string &value) {
        oid(OID{value});
    }

    bool OIDValue::empty() const {
        return false;
    }
}
