//
// Created by dami on 14/06/2018.
//

#ifndef ARKE_DB_STORE_HXX
#define ARKE_DB_STORE_HXX

#include <string>
#include <memory>

#include "../record/RecordPredicate.hxx"
#include "../engine/StoreEngine.hxx"

namespace arke::db {
    class Record;
    class Store {
        private:
            std::unique_ptr<StoreEngine> engine_;

        public:
            explicit Store(std::unique_ptr<StoreEngine> engine);
            RecordPtr get(const std::string& key) const;
            Query<Record> select(std::unique_ptr<const Predicate<Record> > predicate = {}) const;
            void add(const Record& record);
            void update(const Record& record);
            void remove(const std::string& key);
            void foreach(std::function<bool(RecordPtr)> process);
            void foreach(std::function<bool(const RecordPtr)> process) const;
    };

    class InMemoryStore : public Store {
        public:
            InMemoryStore();
    };
}

#endif //ARKE_DB_STORE_HXX
