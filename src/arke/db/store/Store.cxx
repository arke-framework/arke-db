//
// Created by dami on 14/06/2018.
//

#include "Store.hxx"

#include "../record/Record.hxx"
#include "../record/RecordQuery.hxx"
#include "../engine/memory/InMemoryStoreEngine.hxx"

namespace arke::db {

    Store::Store(std::unique_ptr<StoreEngine> engine):
      engine_(std::move(engine)) {}

    RecordPtr Store::get(const std::string& key) const {
        return this->engine_->get(key);
    }

    Query<Record> Store::select(std::unique_ptr<const Predicate<Record> > predicate) const {
        return Query<Record>{this->engine_->getQuery(std::move(predicate))};
    }

    void Store::add(const Record& record) {
        return this->engine_->add(record);
    }

    void Store::update(const Record& record) {
        return this->engine_->update(record);
    }

    void Store::remove(const std::string& key) {
        return this->engine_->remove(key);
    }

    void Store::foreach(std::function<bool(RecordPtr)> process) {
        this->engine_->foreach(process);
    }

    void Store::foreach(std::function<bool(const RecordPtr)> process) const {
        this->engine_->foreach(process);
    }

    InMemoryStore::InMemoryStore():
      Store(std::make_unique<InMemoryStoreEngine>()) {}
}
