//
// Created by Dami on 11/07/2018.
//

#ifndef ARKE_DB_CURSOR_HXX
#define ARKE_DB_CURSOR_HXX

#include <memory>

namespace arke::db {
    template <typename T>
    struct Cursor {
        virtual ~Cursor() {}

        virtual bool isValid() const = 0;
        virtual bool next() = 0;
        virtual std::shared_ptr<T> get() const = 0;
    };
}

#endif //ARKE_DB_CURSOR_HXX
