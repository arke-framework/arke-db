//
// Created by Dami on 11/07/2018.
//

#ifndef ARKE_DB_PREDICATE_HXX
#define ARKE_DB_PREDICATE_HXX

namespace arke::db {
    template <typename T>
    struct Predicate {
        virtual ~Predicate() {}
        virtual bool test(const T& value) const = 0;
    };

    template <typename T>
    std::unique_ptr<const Predicate<T> > predicate(std::function<bool(const T&)> test);
}

#endif //ARKE_DB_PREDICATE_HXX
