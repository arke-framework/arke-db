//
// Created by dami on 13/06/2018.
//

#ifndef ARKE_DB_RECORD_HXX
#define ARKE_DB_RECORD_HXX

#include <string>
#include <memory>

#include "../value/Value.hxx"
#include "../value/ValueType.hxx"

namespace arke::db {
    class Record;
    using RecordPtr = std::shared_ptr<Record>;
    class OID;
    class Record {
        private:
            std::string key_;
            std::shared_ptr<Value> value_;

        public:
            explicit Record(const std::string& key, ValueType type = ValueType::EMPTY);
            explicit Record(const std::string& key, ValuePtr value);
            explicit Record(const std::string& key, const std::string& value);
            explicit Record(const std::string& key, const OID& value, ValueType type = ValueType::OID);

            Record(const Record& record);
            Record(Record&& record);

        public:
            const std::string& key() const;
            const Value& value() const;
            ValuePtr value();
    };
}

#endif //ARKE_DB_RECORD_HXX
