//
// Created by dami on 16/06/2018.
//

#ifndef ARKE_DB_RECORDCURSOR_HXX
#define ARKE_DB_RECORDCURSOR_HXX

#include "../engine/CursorEngine.hxx"
#include "Record.hxx"
#include "../Cursor.hxx"

namespace arke::db {

    template <>
    class Cursor<Record> {
        private:
            std::unique_ptr<CursorEngine> cursorEngine_;

        public:
            explicit Cursor(std::unique_ptr<CursorEngine> cursorEngine):
              cursorEngine_(std::move(cursorEngine)) {}

            bool isValid() const {
                return this->cursorEngine_->isValid();
            }

            bool next() {
                return this->cursorEngine_->next();
            }

            RecordPtr get() const {
                return this->cursorEngine_->get();
            }
    };
}

#endif //ARKE_DB_RECORDCURSOR_HXX
