//
// Created by dami on 14/06/2018.
//

#ifndef ARKE_DB_RECORDPREDICATE_HXX
#define ARKE_DB_RECORDPREDICATE_HXX

#include <memory>
#include <functional>
#include "../Predicate.hxx"

namespace arke::db {

    class Record;

    template <>
    struct Predicate<Record> {
        virtual ~Predicate() {}
        virtual bool test(const Record& record) const {
            return true;
        }
    };

    class LambdaPredicate : public Predicate<Record> {
        private:
            std::function<bool(const Record&)> test_;

        public:
            explicit LambdaPredicate(const std::function<bool(const Record&)>& test):
              test_(test) {}

            virtual bool test(const Record& record) const override {
                return test_(record);
            }
    };

    inline std::unique_ptr<const Predicate<Record> > predicate(std::function<bool(const Record&)> test) {
        return std::make_unique<LambdaPredicate>(test);
    }

    template <>
    inline std::unique_ptr<const Predicate<Record> > predicate(std::function<bool(const Record&)> test) {
        return predicate(test);
    }
}

#endif //ARKE_DB_RECORDPREDICATE_HXX
