//
// Created by dami on 13/06/2018.
//

#include "Record.hxx"
#include "../value/Value.hxx"
#include "../value/EmptyValue.hxx"
#include "../value/StringValue.hxx"
#include "../value/OIDValue.hxx"

namespace arke::db {
    Record::Record(const std::string& key, ValueType type):
      key_(key), value_(std::make_unique<EmptyValue>(type)) {}

    Record::Record(const std::string& key, ValuePtr value):
      key_(key), value_(std::move(value)) {}

    Record::Record(const std::string& key, const std::string& value):
      key_(key), value_(std::make_unique<StringValue>(value)) {}

    Record::Record(const std::string& key, const OID& value, ValueType type):
      key_(key), value_(std::make_unique<OIDValue>(value, type)) {}

    Record::Record(const Record &record):
      key_(record.key_), value_(clone(*record.value_)) {}

    Record::Record(Record &&record):
      key_(std::move(record.key_)), value_(std::move(record.value_)) {}

    const std::string& Record::key() const {
        return key_;
    }

    const Value& Record::value() const {
        return *value_;
    }

    ValuePtr Record::value() {
        return value_;
    }
}
