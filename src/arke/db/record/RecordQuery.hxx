//
// Created by dami on 14/06/2018.
//

#ifndef ARKE_DB_RECORDQUERY_HXX
#define ARKE_DB_RECORDQUERY_HXX

#include <cstddef>
#include <memory>
#include "../engine/QueryEngine.hxx"
#include "../Query.hxx"
#include "RecordCursor.hxx"

namespace arke::db {

    template <>
    class Query<Record> {
        private:
            std::unique_ptr<QueryEngine> engine_;

        public:
            explicit Query(std::unique_ptr<QueryEngine> engine):
              engine_(std::move(engine)) {}

            size_t count() const {
                return engine_->count();
            }

            Cursor<Record> cursor() const {
                return Cursor<Record>(engine_->getCursor());
            }
    };
}

#endif //ARKE_DB_RECORDQUERY_HXX
