//
// Created by Dami on 11/07/2018.
//

#ifndef ARKE_DB_QUERY_HXX
#define ARKE_DB_QUERY_HXX

#include <cstddef>

#include "Cursor.hxx"

namespace arke::db {

    template <typename T>
    struct Query {
        virtual ~Query() {}

        virtual size_t count() const;
        virtual Cursor<T> cursor() const;
    };

}

#endif //ARKE_DB_QUERY_HXX
