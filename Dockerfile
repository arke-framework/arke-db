FROM creabox/cxx-build

RUN conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
RUN conan install boost_system/1.66.0@bincrafters/stable
RUN conan install boost_uuid/1.66.0@bincrafters/stable
RUN conan install catch2/2.2.2@bincrafters/stable
RUN apt update && apt install -y lcov
