#!/bin/bash
PROJECT_DIRECTORY=$(cd "$(dirname "$0")/.."; pwd)
BUILD_DIRECTORY="$PROJECT_DIRECTORY/cmake-build-coverage"
mkdir ${BUILD_DIRECTORY}
pushd ${BUILD_DIRECTORY}
conan install .. --build=missing
cmake -DCOVERAGE=ON ..
make clean
find . -name "*.gcda" | xargs rm
cmake --build .
ctest .
${PROJECT_DIRECTORY}/scripts/coverage.sh
popd
