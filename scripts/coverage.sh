#!/bin/bash
lcov --directory . --capture --output-file coverage.info && \
    lcov --remove coverage.info '*/usr/*' \
        -r coverage.info "*/.conan/*" \
        -r coverage.info "*/test/*" \
        --output-file coverage.info && \
    lcov --list coverage.info && \
    genhtml coverage.info
